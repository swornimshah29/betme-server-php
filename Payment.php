<?php

//deals with all payment operation

require_once('./Connection.php');
require_once('./Status.php');

class Payment{
    
        var $to;//receiver
        var $from;//sender
        var $sync;//not needed for server side
        var $amount;//amount to be
        var $paymentId;//uniqueid of the paymentcolumn
        var $paymentStatus;//payed or not payed

    function clearLoanBySender($connection){
        //this is called by sender to clear the loan

        //dont put where paymentStatus='loan' because receiver might have updated before the sender soo sender will always send this data 
        //even its not a problem
        if(mysqli_query($connection,"UPDATE paymenttest SET paymentStatus='payed' WHERE paymentId='$this->paymentId' AND froom='$this->from' ")){
            //sender only sends one number throught out his app 
            //so no need to verify both the numbers

            return 'success';
        }else{
            return 'failed';
        }
    }

    function clearLoanByReceiver($connection){
        //this is called by receiver to clear the loan of the person who paid
        //if that person had two numbers and if another number is used as balance trasnfer then checking both the numbers

        if(mysqli_query($connection,"UPDATE paymenttest SET paymentStatus='payed' WHERE amount='$this->amount' AND froom='$this->to' AND too='$this->from' ")){
            //sender only sends one number throught out his app 
            //so no need to verify both the numbers
            return 'success';
        }else{
            return 'failed';
        }
    }


    function hasLoan($connection){
        //to check if the user has a loan or not if yes then return the json structure
        //this json gets saved in the database offline and everytime client calls these method
        //json is responded and database is again overrided and may be next time data might change
        //soo this is excellent method


    if($loanResult=mysqli_query($connection,"SELECT * from paymenttest where froom ='$this->from' AND paymentStatus='loan' ")){
        $loans=array();
        while($each=mysqli_fetch_assoc($loanResult)){
            $payment=new Payment;
            $payment->amount=$each['amount'];
            $payment->paymentStatus=$each['paymentStatus'];
            $payment->to=$each['too'];
            $payment->from=$each['froom'];
            $payment->paymentId=$each['paymentId'];
            $payment->sync="false";
            
            //sync is sent as null
            $loans[]=$payment;
                            
        }
        return $loans;
    }else{
        return 'failed';
    }

}

}


//check if i have got loan
if(isset($_POST['userPhoneNumber'])){

    $connection=new Connection('localhost','root','','betme');
    $connection=$connection->connect();

    $loanResult=mysqli_query($connection,"SELECT * FROM betting");
}


//this is for the sender who just paid because it has got paymentId
//clearloan is extra variable to differenciate functions calls
if(isset($_POST['from']) && isset($_POST['paymentId']) && isset($_POST['amount']) && isset($_POST['paymentStatus']) && isset($_POST['to']) && isset($_POST['paymentBySender'])){
    
    $status=new Status;
    $payment=new Payment;    
    $connection=new Connection('localhost','root','','betme');
    $connection=$connection->connect();
    if($connection){
        //clear that paymentId loan
        $payment->from=$_POST['from'];
        $payment->amount=$_POST['amount'];
        $payment->to=$_POST['to'];
        $payment->paymentStatus=$_POST['paymentStatus'];
        $payment->paymentId=$_POST['paymentId'];
        if($payment->clearLoanBySender($connection)=='success'){
            $status->statusCode='success';
            $status->statusType='payment';
            echo json_encode($status);
        }else{
            $status->statusCode='failed';
            $status->statusType='payment';
            echo json_encode($status);
        }

    }else{
        $status->statusCode='failed';
        $status->statusType='payment';
        echo json_encode($status);
    }

}


if(isset($_POST['from']) && isset($_POST['paymentId']) && isset($_POST['amount']) && isset($_POST['paymentStatus']) && isset($_POST['to']) && isset($_POST['paymentByReceiver'])){

    //from means one who got the balance 
    //to means one who sent soo receiver will clear the 'to' column i.e 'from column will be used'
    
    $status=new Status;
    $payment=new Payment;    
    $connection=new Connection('localhost','root','','betme');
    $connection=$connection->connect();
    if($connection){
        //clear that paymentId loan
        $payment->from=$_POST['from'];
        $payment->amount=$_POST['amount'];//this will be id for comparing
        $payment->to=$_POST['to'];
        $payment->paymentStatus=$_POST['paymentStatus'];
        $payment->paymentId=$_POST['paymentId'];//no need to  compare using this
        if($payment->clearLoanByReceiver($connection)=='success'){
            $status->statusCode='success';
            $status->statusType='payment';
            echo json_encode($status);
        }else{
            $status->statusCode='failed';
            $status->statusType='payment';
            echo json_encode($status);
        }

    }else{
        $status->statusCode='failed';
        $status->statusType='payment';
        echo json_encode($status);
    }

}


//this is the request for loans so to avoid clear loan call because
//from is set for above if code as well
if(isset($_POST['from']) && isset($_POST['hasLoan'])){
    $connection=new Connection('localhost','root','','betme');
    $connection=$connection->connect();
    $status=new Status;
    $payment=new Payment;    
    $connection=new Connection('localhost','root','','betme');
    $connection=$connection->connect();
    if($connection){
        //clear that paymentId loan
        $payment->from=$_POST['from'];        
        $loans=$payment->hasLoan($connection);
        if(is_array($loans)){

            echo json_encode(array('loans'=>$loans));
        }else{
            echo 'failed';
        }
    }else{
        $status->statusCode='failed';
        $status->statusType='payment';
        echo json_encode($status);
    }

}




?>