<?php
/*This handles all the registration process */
require_once('./Connection.php');
require_once('./Status.php');
require_once('./Synchronization.php');

class Registration{

    var $userPhoneNumber;
    var $userName;
    var $id;//table id primary key
    var $friendJson;

    function __construct($userName,$userPhoneNumber,$friendJson){
        $this->userName=$userName;
        $this->userPhoneNumber=$userPhoneNumber;
        $this->friendJson=$friendJson;
        
        
    }    

    function alreadyRegistered($connection){

    $result=mysqli_query($connection,"SELECT * FROM registeredusers");
    while($eachrows=mysqli_fetch_assoc($result)){
        if($this->userPhoneNumber==$eachrows['userPhoneNumber']){
            return true;
        }
    }
       return false; 
    }

    function makeRegisterWithoutContacts($connection){
      $result=mysqli_query($connection,
      "INSERT INTO `registeredusers` (userPhoneNumber,userName)  VALUES('$this->userPhoneNumber','$this->userName')");

      /*save to the database as friendjson as well next time client dont have to sent again the same data*/

      if($result){
        $statusObject=new Status;
        $statusObject->statusCode='success';
        $statusObject->statusType='registration';
        echo json_encode($statusObject);

        //Broadcast  youself that you have installed application to friends
        $friendObject=new Friends;
        $friendObject->synchronizeHasAppByUser($connection,$this->userPhoneNumber);
                
        }else{
            $statusObject=new Status;
            $statusObject->statusCode='failed';
            $statusObject->statusType='registration';
            echo json_encode($statusObject);
        }

        
    }

    function makeRegisterWithContacts($connection){
                
              $result=mysqli_query($connection,
              "UPDATE registeredusers SET friendJson='$this->friendJson' WHERE userPhoneNumber='$this->userPhoneNumber' ");
        
              /*save to the database as friendjson as well next time client dont have to sent again the same data*/
            }

    function makeFriendsArray($friendJson){

        // Convert JSON string to Array
        $friends= json_decode($friendJson, true);
        for($i=0;$i<sizeof($friends);$i++){

            $eachFriend=new Friends;
            $eachFriend->friendNumber=$friends[$i]['friendNumber']; 
            $eachFriend->friendName=$friends[$i]['friendName']; 
            $eachFriend->hasApp=$friends[$i]['hasApp']; 
            $eachFriend->isFriend=$friends[$i]['isFriend']; 
            $eachFriend->added=$friends[$i]['added']; 
            $eachFriend->friendOf=$friends[$i]['friendOf']; 
            
            $friends=$eachFriend;
        }

       return $friends;
    }



}


if(isset($_POST['userPhoneNumber']) && isset($_POST['userName']) && isset($_POST['registerWithoutContacts'])){

    $connection=new Connection('localhost','root','','betme');
    $connection=$connection->connect();

    //input user profiles
    $registrationObject=new Registration($_POST['userName'],$_POST['userPhoneNumber'],NULL);
    if($registrationObject->alreadyRegistered($connection)){

        $status=new Status;
        $status->statusType='registration';
        $status->statusCode='already';
        echo json_encode($status);

    }else{
        $registerStatus=$registrationObject->makeRegisterWithoutContacts($connection);
    }

}

if(isset($_POST['userPhoneNumber']) && isset($_POST['registerWithContacts']) && isset($_POST['friendJson'])){
    
        $connection=new Connection('localhost','root','','betme');
        $connection=$connection->connect();
        $registrationObject=new Registration(NULL,$_POST['userPhoneNumber'],$_POST['friendJson']);
        $friends=json_decode($_POST['friendJson'],true);

        $duplicatesSql=mysqli_query($connection,"SELECT count(*) as total FROM friends WHERE friendOf='$registrationObject->userPhoneNumber' ");
        $count=mysqli_fetch_assoc($duplicatesSql);
        if($count['total']>0){
            $status=new Status;
            $status->statusCode='success';
            $status->statusType='friendsAdded';
            echo json_encode($status);
        }else{

            for($i=0;$i<sizeof($friends);$i++){
                
                $eachFriend=new Friends;
                $eachFriend->friendNumber=$friends[$i]['friendNumber']; 
                $eachFriend->friendName=$friends[$i]['friendName']; 
                $eachFriend->hasApp=$friends[$i]['hasApp']; 
                $eachFriend->isFriend=$friends[$i]['isFriend']; 
                $eachFriend->added=$friends[$i]['added']; 
                $eachFriend->friendOf=$friends[$i]['friendOf']; 
                                
                $result=mysqli_query($connection,
                "INSERT INTO `friends` (friendNumber,friendName,isFriend,added,friendOf,hasApp) 
                VALUES('$eachFriend->friendNumber','$eachFriend->friendName','$eachFriend->isFriend','$eachFriend->added','$eachFriend->friendOf','$eachFriend->hasApp') ");
            }
    
            $status=new Status;
            $status->statusCode='success';
            $status->statusType='friendsAdded';
            echo json_encode($status);
        }


    }
       
    
    
    

    


?>