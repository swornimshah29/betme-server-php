<?php
/*This class handles the connection of the client to the database*/
class Connection{
    var $host;
    var $sqlname;
    var $sqlpassword;
    var $database;
    
    
    
    function __construct($host,$sqlname,$sqlpassword,$database){
        $this->host=$host;
        $this->sqlname=$sqlname;
        $this->sqlpassword=$sqlpassword;
        $this->database=$database;
        
    }

    function connect(){
        $connection=mysqli_connect($this->host,$this->sqlname,$this->sqlpassword,$this->database);
        return $connection;

    }

}





?>