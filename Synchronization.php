<?php
/*This class helps in the synchronization process */
require_once('./Connection.php');
require_once('./Status.php');

class Friends{

    var $friendNumber;
    var $friendName;
    var $hasApp;//whenever user registers they update in all the contacts of there friends
    var $isFriend;//true means: receiver also added u back
    var $friendOf;//whose friend is this of
    var $added;//true means: user added him but not vice versa(UI:request sent)
    function checkNewFriend($connection){

        //new recommendation on every call

        $recommendations=array();
        $newFriendSql=mysqli_query($connection,"SELECT * FROM friends WHERE hasApp='yes' ");
        while($each=mysqli_query($newFriendSql)){
        
            $eachObject=new Friends;
            $eachObject->friendNumber=$each['friendNumber'];
            $eachObject->friendName=$each['friendName'];
            $eachObject->hasApp=$each['hasApp'];
            $eachObject->isFriend=$each['isFriend'];
            $eachObject->added=$each['added'];
            $eachObject->friendOf=$each['friendOf'];
            $recommendations[]=$eachObject;
        }
        
        echo json_encode($recommendations);


    }

    function addFriend($connection){
        //add request sent to friend

        $updateOwn=mysqli_query($connection,
        "UPDATE  friends SET isFriend='$this->isFriend',added='$this->added' WHERE friendNumber='$this->friendNumber' AND friendOf='$this->friendOf' ");//update own for requesnt_sent (UI)
   

        /*by default user will sent added:yes isfriend:no*/

        $sentRequest=mysqli_query($connection,
        "UPDATE  friends SET isFriend='$this->added',added='$this->isFriend' WHERE friendNumber='$this->friendOf' AND friendOf='$this->friendNumber' ");//update friend for confirm (UI)


        if($updateOwn && $sentRequest){
            //if both the query completes then only send success
            return 'success';
        }else{
            return 'failed';
        }

    }

    function synchronizeHasAppByUser($connection,$userNumber){
        //go to all rows and find urself in friendNumber and update as hasApp:yes
        //this gets called by the new user who registered and by the admin for other left users periodically

        $updateHasApp=mysqli_query($connection,
        "UPDATE friends SET hasApp='yes' WHERE friendNumber='$userNumber' ");

        if($updateHasApp){
            return 'success';
         
        }else{
            return 'failed';
        }

    }

    function synchronizeHasAppByAdmin($connection,$userNumber){
        //go to all rows and find urself in friendNumber and update as hasApp:yes
        //this gets called by the new user who registered and by the admin for other left users periodically

        $updateHasApp=mysqli_query($connection,
        "UPDATE friends SET hasApp='yes' WHERE friendNumber='$userNumber' ");

        if($updateHasApp){
            echo 'success';
        }else{
            echo 'Failed for: '.$userNumber;
        }

    }


    //client sends new friend in array
    function addNewContact($connection,$newContacts){

    }






}
// $connection=new Connection('localhost','root','','betme');
// $connection=$connection->connect();
// $recommendations=array();
// $newFriendSql=mysqli_query($connection,"SELECT * FROM friends WHERE hasApp='yes' ");
// while($each=mysqli_fetch_assoc($newFriendSql)){

//     $eachObject=new Friends;
//     $eachObject->friendNumber=$each['friendNumber'];
//     $eachObject->friendName=$each['friendName'];
//     $eachObject->hasApp=$each['hasApp'];
//     $eachObject->isFriend=$each['isFriend'];
//     $eachObject->added=$each['added'];
//     $eachObject->friendOf=$each['friendOf'];
//     $recommendations[]=$eachObject;
// }

// //if no data send blank as array type or else datas
// echo json_encode($recommendations);


// $friendObject=new Friends;
// if($friendObject->addFriend($connection)=='success'){

//     $statusObject=new Status;
//     $statusObject->statusCode='success';
//     $statusObject->statusType='synchronization';
//     echo json_encode($statusObject);

// }else{
//     $statusObject=new Status;
//     $statusObject->statusCode='failed';
//     $statusObject->statusType='synchronization';
//     echo json_encode($statusObject);
// }


?>

