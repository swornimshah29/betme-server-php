<?php
require_once('./Connection.php');

// echo (file_get_contents('./Newsfeed.json'));


class Newsfeed{
  var  $userName;
  var  $userNumber;
  var  $opponentNumber;
  var  $opponentName;
  var  $leftTeamName;
  var  $rightTeamName;
  var  $userTeamSelected;
  var  $opponentTeamSelected;
  var  $amount;
  var  $eventDate;
  var  $message;//random variation of messages
  var $timeStamp;
    
 

}

// $diff = $later->diff($earlier)->format("%a");
// echo $diff;


if(isset($_POST['userPhoneNumber']) && isset($_POST['newsFeed'])){
    $connection=new Connection('localhost','root','','betme');
    $connection=$connection->connect();

    $newsfeedObject=new Newsfeed;
    $newsfeedObject->userNumber=$_POST['userPhoneNumber'];
    $mChallengeNews=NULL;
    
    $mChallengeNotFixedSql=mysqli_query($connection,
    "SELECT * FROM challenge WHERE userNumber!='$newsfeedObject->userNumber' AND 
    opponentTeamSelected IS NULL AND DATEDIFF(eventDate,date(now()))<=3 AND DATEDIFF(eventDate,date(now()))>=-3 ");

    $mChallengeFixedSql=mysqli_query($connection,
    "SELECT * FROM challenge WHERE userNumber!='$newsfeedObject->userNumber' AND 
    opponentTeamSelected IS NOT NULL AND DATEDIFF(eventDate,date(now()))<=3 AND DATEDIFF(eventDate,date(now()))>=-3  ");
    
    
    while($eachMessage=mysqli_fetch_assoc($mChallengeNotFixedSql)){
        $newsfeedObject=new Newsfeed;        
        $newsfeedObject->userName=$eachMessage['userName'];
        $newsfeedObject->userNumber=$eachMessage['userNumber'];
        $newsfeedObject->opponentNumber=$eachMessage['opponentNumber'];
        $newsfeedObject->opponentName=$eachMessage['opponentName'];
        $newsfeedObject->leftTeamName=$eachMessage['leftTeamName'];
        $newsfeedObject->rightTeamName=$eachMessage['rightTeamName'];
        $newsfeedObject->userTeamSelected=$eachMessage['userTeamSelected'];
        $newsfeedObject->opponentTeamSelected=$eachMessage['opponentTeamSelected'];
        $newsfeedObject->amount=$eachMessage['amount'];
        $newsfeedObject->eventDate=$eachMessage['eventDate'];
        $newsfeedObject->timeStamp=$eachMessage['timeStamp'];
        
        $newsfeedObject->message=$eachMessage['userName']." challenged ".$eachMessage['opponentName']. 'for Rs.'.$eachMessage['amount'];
        $mChallengeNews[]=$newsfeedObject;
        
    }

    while($eachMessage=mysqli_fetch_assoc($mChallengeFixedSql)){
        $newsfeedObject=new Newsfeed;
        
        $newsfeedObject->userName=$eachMessage['userName'];
        $newsfeedObject->userNumber=$eachMessage['userNumber'];
        $newsfeedObject->opponentNumber=$eachMessage['opponentNumber'];
        $newsfeedObject->opponentName=$eachMessage['opponentName'];
        $newsfeedObject->leftTeamName=$eachMessage['leftTeamName'];
        $newsfeedObject->rightTeamName=$eachMessage['rightTeamName'];
        $newsfeedObject->userTeamSelected=$eachMessage['userTeamSelected'];
        $newsfeedObject->opponentTeamSelected=$eachMessage['opponentTeamSelected'];
        $newsfeedObject->amount=$eachMessage['amount'];
        $newsfeedObject->eventDate=$eachMessage['eventDate'];
        $newsfeedObject->timeStamp=$eachMessage['timeStamp'];        
        $newsfeedObject->message=$eachMessage['userName']." challenge was accepted by ".$eachMessage['opponentName']. 'for Rs.'.$eachMessage['amount'];
        $mChallengeNews[]=$newsfeedObject;
        
        
    }

    if($mChallengeFixedSql && $mChallengeNotFixedSql){        
        if($mChallengeNews==NULL){
            //to avoid null point exception in the client side
            $mChallengeNews=[];
            echo json_encode($mChallengeNews);
        }else{
            echo json_encode($mChallengeNews);
            
        }
    }

}
// $connection=new Connection('localhost','root','','betme');
// $connection=$connection->connect();
// $date=new DateTime('2018/06/06');
// $upperLimit= date('Y-m-d',strtotime('2018/06/06'));

//$datesql=mysqli_query($connection,$query);

//$result=mysqli_fetch_assoc($datesql);
//echo $result['total']; 

// echo 'today'. date('Y/m/d')."\n";//todays date
// $upperLimit= date('Y-m-d',strtotime('2018/06/06 +2 days'));
// echo $upperLimit;
// select * from mytable where date between '10-11-2010' and '17-11-2010'
// $sql = mysqli_query($connection,"SELECT * FROM challenge where DATEDIFF(eventDate,date(now()))<=2 OR DATEDIFF(eventDate,date(now()))>=2");

// if($sql){
//     echo 'success'."</br>";
// }


// while($each=mysqli_fetch_assoc($sql)){
//     echo $each['userName']."</br>";
// }


?>