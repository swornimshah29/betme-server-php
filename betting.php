<?php
/*This script deals with the betting related operations*/
require_once('./Connection.php');
require_once('./Status.php');


class Bets{

    function __construct($userPhoneNumber,$eventId,$eventJson){
        $this->userPhoneNumber=$userPhoneNumber;
        $this->eventId=$eventId;
        $this->eventJson=$eventJson;        
    }

    function alreadyBet($connection){

    $result=mysqli_query($connection->connect(),
    "SELECT * FROM betting where userPhoneNumber='$this->userPhoneNumber' AND eventId='$this->eventId' ");
    if(mysqli_num_rows($result)){
        return true;
    }

}


function makeBet($connection){
    $pending="pending";
    $result=mysqli_query($connection->connect(),
    "INSERT INTO betting (userPhoneNumber,eventId,eventJson,betStatus)
    VALUES('$this->userPhoneNumber','$this->eventId','$this->eventJson','$pending') ");
     if($result){
        return true;
    }else{
        return false;
    }

}




    function bettingStatus($connection){
        //return the entire betting news including betted and pending or expired as a Notification class
    }


}

class Challenge{
   
   var $userName;
   var $userNumber;
   var $tick;
   var $opponentNumber;
   var $opponentName;
   var $leftTeamName;
   var $rightTeamName;
   var $userTeamSelected;
   var $opponentTeamSelected;
   var $amount;
   var $eventDate;
   var $challengeType;
   var $challengeStatus;
   var $eventId;



   function alreadyChallenged($connection,$object){

    $alreadyChallengeSql=mysqli_query($connection,"SELECT count(*) as total FROM challenge WHERE eventId='$object->eventId' AND userNumber='$object->userNumber' ");
    $counter=mysqli_fetch_assoc($alreadyChallengeSql);
    if($counter['total']>0){
        //already betted for that challenge event
        return 'true';
    }
    return 'false';

   }

   function challengeFriend($connection,$object){
       
    $result=mysqli_query($connection,
    "INSERT INTO `challenge` (userName,userNumber,tick,opponentNumber,opponentName,
    leftTeamName,rightTeamName,userTeamSelected,amount,eventDate,challengeType,challengeStatus,eventId)
    VALUES('$object->userName','$object->userNumber','$object->tick','$object->opponentNumber','$object->opponentName','$object->leftTeamName',
    '$object->rightTeamName','$object->userTeamSelected','$object->amount','$object->eventDate','$object->challengeType',
    '$object->challengeStatus','$object->eventId') ");

    if($result){
        return 'success';
    }else{
        return 'failed';
    }

}







}


if(isset($_POST['eventJson']) && isset($_POST['userPhoneNumber']) && isset($_POST['eventId'])){
    //entry point of the code

    $betObject=new Bets($_POST['userPhoneNumber'],$_POST['eventId'],$_POST['eventJson']);
    $connection=new Connection('localhost','root','','betme');
    $status=new Status;
    if($betObject->alreadyBet($connection)){
        $status->statusType="betting";
        $status->statusCode="already";
        echo json_encode($status);
    }else{
        //make the betting
        if($betObject->makeBet($connection)){
            $status->statusType="betting";
            $status->statusCode="success";
            echo json_encode($status);
        }else{
            $status->statusType="betting";
            $status->statusCode="failed";
            echo json_encode($status);
        }
    }
}

  
    if(isset($_POST['challengeJson']) && isset($_POST['userPhoneNumber'])){

        $connection=new Connection('localhost','root','','betme');
        $connection=$connection->connect();
        $challengeObject=new Challenge;
        $status=new Status;
        
        $object=json_decode($_POST['challengeJson']);//object
        if($challengeObject->alreadyChallenged($connection,$object)=='true'){
            $status->statusType="challenge";
            $status->statusCode="already";
            echo json_encode($status);
        }else{
            //make the challenge
            if($challengeObject->challengeFriend($connection,$object)=='success'){
                $status->statusType="challenge";
                $status->statusCode="success";
                echo json_encode($status);
            }else{
                $status->statusType="challenge";
                $status->statusCode="failed";
                echo json_encode($status);
            }
            
        }

    }


?>